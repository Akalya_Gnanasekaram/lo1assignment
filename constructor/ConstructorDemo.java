package Ap.qus.constructor;

public class ConstructorDemo {

	public static void main(String[] args) {

		Student Akal = new Student("Akal");
		Akal.printDetails();

		Student Thaanu = new Student("Thaanu");
		Thaanu.printDetails();

		Student Milux = new Student("Milux");
		Milux.printDetails();

		Student niro = new Student("niro");
		niro.printDetails();
	}
}
