
class StaticBlock {
	static int x = 25;
	static double y = 15;
	static double z;
	static double tot;

	// static blocks
	static {
		System.out.println("First Static block.");
		z = x * y;
	}
	static {
		System.out.println("Second Static block.");
		tot = z;
	}

	// static method
	static void display() {

		System.out.println("x = " + x);
		System.out.println("y = " + y);
		System.out.println("Total = " + tot);
	}

	public static void main(String args[]) {
		// calling the static method
		display();
	}
}
